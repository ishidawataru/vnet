# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'vnet/version'

Gem::Specification.new do |spec|
  spec.name          = "vnet"
  spec.version       = Vnet::VERSION
  spec.authors       = ["Wataru ISHIDA"]
  spec.email         = ["ishida.wataru@lab.ntt.co.jp"]
  spec.description   = %q{vnet: supports virtual network construction}
  spec.summary       = %q{vnet}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]
  
  spec.add_dependency "systemu"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
end
