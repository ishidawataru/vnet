require 'spec_helper'

describe Vnet::Switch::OVS do
    it 'start' do
        Vnet::clear
        puts "finished clear."
        ovs01 = Switch::mkBridge :ovs, :ovs01
        ovs01.setController IPAddr.new("192.168.0.2")
        ovs02 = Switch::mkBridge :ovs, :ovs02
        ovs02.setController IPAddr.new("192.168.0.3")
        veth1, veth2 = Vnet.mkLink
        ovs01.addif veth1
        ovs02.addif veth2
        veth1, veth2 = Vnet.mkLink
        ovs01.addif veth1
        ovs02.addif veth2
        Switch::run

        p Switch::OVS.getif
    end
end
