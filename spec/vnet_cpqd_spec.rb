require 'spec_helper'

describe Vnet::Switch::CPqD do
    Switch = Vnet::Switch
    CPqD = Vnet::Switch::CPqD
    it 'check' do
        log.level = Logger::INFO
        CPqD::check.should eq(true)
    end
    it 'load' do
        CPqD::load
        Switch::getbr.each do |k,v|
            p k
            p v.getif.keys
            p v.ctrladdr
            p v.scport
        end
    end
    it 'check running' do
        Switch::clear
    end
    it 'start' do
        Vnet::clear
        puts "finished clear."
        cpqd01 = Switch::mkBridge :cpqd, :cpqd01
        cpqd01.setController IPAddr.new("192.168.0.2")
        cpqd02 = Switch::mkBridge :cpqd, :cpqd02
        cpqd02.setController IPAddr.new("192.168.0.3")
        veth1, veth2 = Vnet.mkLink
        cpqd01.addif veth1
        cpqd02.addif veth2
        veth1, veth2 = Vnet.mkLink
        cpqd01.addif veth1
        cpqd02.addif veth2
        Switch::run
    end

    it 'ofctl' do
        Switch::getbr.each do |k, v|
            p k
            p v.dumpflows
        end
    end

end
