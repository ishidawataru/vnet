require 'spec_helper'

describe Vnet::IPAddr do
    it 'represent ipv4 address' do
        ip = Vnet::IPAddr.new("10.0.0.10/24")
        expect(ip.to_s).to eq("10.0.0.10/24")
        expect(ip.network.cidr).to eq(24)
    end

    it 'include? tells weather ip is in ip' do
        ip = Vnet::IPAddr.new("10.0.0.10/24")
        ip2 = Vnet::IPAddr.new("10.0.10.10/24")
        ip3 = Vnet::IPAddr.new("10.0.0.20/24")
        expect(ip.include? ip2).to eq(false)
        expect(ip.include? ip3).to eq(true)
    end
    
    it 'cidr return 24 when netmask is 24' do
        expect(Vnet::IPAddr.new("192.168.0.0/24").cidr).to eq(24)
    end

    it 'network should return network address' do
        expect(Vnet::IPAddr.new("192.168.0.10/24").network.to_s).to eq("192.168.0.0/24")
    end
    
    it 'broadcast should return broadcast address' do
        expect(Vnet::IPAddr.new("192.168.0.0/24").broadcast.to_s).to eq("192.168.0.255/24")
    end
end
