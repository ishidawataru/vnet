require 'spec_helper'

describe Vnet::System do
    include Vnet::System
    before(:each) do
        clear
        p "finished clear."
    end

    it 'should have a version number' do
        Vnet::VERSION.should_not be_nil
    end

    it 'execinNamespace can run command' do
        result = exec'echo hello'
        result.should eq('hello')
    end

    it 'execinNamespace can report failed command' do
        proc {
            exec 'false'
        }.should raise_error(SystemError)
    end

    it 'execinNamespace can report invalid namespace' do
        proc {
            exec 'true', :invalid_ns
        }.should raise_error(ArgumentError)
    end
   
    it 'getns should return list' do
        addns(:ns01)
        addns(:ns02)
        Set.new(getns.keys).should eq(Set.new([:default, :ns01, :ns02]))
    end

    it 'getif should return list' do
        addns(:ns01)
        addif(:veth1, :veth2)
        addif(:veth3, :veth4)
        setns(:veth1, :ns01)
        setns(:veth3, :ns01)
        Set.new(getif.keys).should \
            eq(Set.new([:veth1, :veth2, :veth3, :veth4]))
    end

    it 'getbr should show br info' do
        addbr(:br01)
        addif(:veth1, :veth2)
        setbr(:veth1, :br01)
        setbr(:veth2, :br01)
        getbr.should eq({:br01=>[:veth1, :veth2]})
    end

    it 'getif can get address info' do
        addif(:veth1, :veth2)
        nw = Vnet::Network.new("192.168.0.0/24")
        setaddr(:veth1, nw.nextaddr.to_s)
        setaddr(:veth1, nw.nextaddr.to_s)
        setaddr(:veth2, nw.nextaddr.to_s)
        info = getif
        info[:veth1][:addrs].should \
            eq(["192.168.0.1/24", "192.168.0.2/24"])
        info[:veth2][:addrs].should \
            eq(["192.168.0.3/24"])
    end

end
