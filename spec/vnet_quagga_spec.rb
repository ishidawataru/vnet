require 'spec_helper'

describe Vnet::Quagga do
    Quagga = Vnet::Quagga

    it 'check' do
        log.level = Logger::DEBUG
        Quagga::check.should be_true
    end

    before(:each) do
        Vnet::Quagga.clear
        Vnet.clear
        ns = Namespace.new :ns01
        veth1, veth2 = Vnet.mkLink
        ns.addif veth1
    end

    it 'should run zebra' do
        zebra = Vnet::Quagga::Zebra.new :ns01
        zebra.write
        zebra.run
    end

    it 'should run ospfd' do
        ospfd = Vnet::Quagga::OSPFd.new :ns01
        ospfd.write
        ospfd.run
        ospfd.die
    end
    
    it 'should run bgpd' do
        bgpd = Vnet::Quagga::BGPd.new :ns01, 1000
        bgpd.write
        bgpd.run
        bgpd.die
    end

    it 'OSPFd should accept interface addition' do
        ospfd = Vnet::Quagga::OSPFd.new :ns01
        Vnet.nss[:ns01].getif.each do |veth|
            veth.setaddr Vnet::IPAddr.new("10.10.10.1/30")
            ospfd.addif veth
        end
        ospfd.write
        ospfd.run
        ospfd.die
    end

    it 'OSPFd should accept advatising network' do
        ospfd = Vnet::Quagga::OSPFd.new :ns01
        ospfd.redistribute(:bgp)
        Vnet.nss[:ns01].getif.each do |veth|
            nw = Vnet::Network.new "10.10.10.0/30"
            veth.setaddr nw.nextaddr
            ospfd.addif veth
            ospfd.addnw nw.ip
        end
        ospfd.write
        ospfd.run
        ospfd.die
    end

    it 'BGPd should accept advatising network' do
        bgpd = Vnet::Quagga::BGPd.new :ns01, 1000
        Vnet.nss[:ns01].getif.each do |veth|
            nw = Vnet::Network.new "10.10.10.0/30"
            bgpd.addnw nw.ip
        end
        bgpd.write
        bgpd.run
        bgpd.die
    end

    it 'BGPd should accept neighbor' do
        bgpd1 = Vnet::Quagga::BGPd.new :ns01, 1000
        bgpd2 = Vnet::Quagga::BGPd.new :ns01, 2000
        Vnet.nss[:ns01].getif.each do |veth|
            nw = Vnet::Network.new "10.10.10.0/30"
            bgpd1.addneighbor(nw.nextaddr, bgpd2.asn)
        end
        bgpd1.write
        bgpd1.run
        bgpd1.die
    end
end

