require 'spec_helper'

describe Vnet do
    before(:each) do
        Vnet.clear
        p "finished clear."
    end

    it 'create namespace' do
        ns01 = Namespace.new :ns01
        veth1, veth2 = Vnet.mkLink
        ns01.addif veth1
        ns01.getif.should eq([veth1])
    end

    it 'connect should connect two namespaces' do
        ns01 = Namespace.new :ns01
        ns02 = Namespace.new :ns02
        veth1, veth2 = Vnet.mkLink
        veth1.setns(ns01)
        veth2.setns(ns02)
        veth1.getns.should eq(ns01)
        veth2.getns.should eq(ns02)
    end

    it 'set address' do
        veth1, veth2 = Vnet.mkLink
        nw = Vnet::Network.new("192.168.0.0/24")
        addrs = []
        addr = nw.nextaddr
        addrs << addr
        veth1.setaddr(addr)
        addr = nw.nextaddr
        veth1.setaddr(addr)
        addrs << addr
        veth1.addrs.should eq(addrs)
    end

    it 'create bridge' do
        br1 = Bridge.new :br01
        veth1, veth2 = Vnet.mkLink
        br1.addif veth1
        br1.addif veth2
        veth1.getbr.should eq(br1)
        Set.new(br1.getif).should eq(Set.new([veth1, veth2]))
    end
end
