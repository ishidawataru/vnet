require 'spec_helper'

describe Vnet::Network do
    it 'nextaddr returns next ip address in the network' do
        nw = Vnet::Network.new "10.10.10.0/30"
#        expect(nw.nextaddr).to eq("10.10.10.1")
#        expect(nw.nextaddr).to eq("10.10.10.2")
    end
    
    it 'raise exception when addr is not network addr' do
        expect{
            nw = Vnet::Network.new "10.10.10.1/30"
        }.to raise_error
    end

    it 'nextaddr raise exception when there are no more address' do
        nw = Vnet::Network.new "10.10.10.0/30"
        expect(nw.nextaddr.to_s).to eq("10.10.10.1/30")
        expect(nw.nextaddr.to_s).to eq("10.10.10.2/30")
        expect{
            nw.nextaddr
        }.to raise_error
    end
end
