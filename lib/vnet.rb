require "vnet/version"
require 'vnet/system'
require 'vnet/core'
require 'vnet/switch/switch'
require 'vnet/switch/ovs'
require 'vnet/switch/ovs13'
require 'vnet/switch/cpqd'
require 'vnet/network'
require 'vnet/quagga/quagga'

module Vnet
    class SystemError < StandardError; end
    class ExecError < SystemError
        def initialize(cmd, stdout, stderr)
            @cmd = cmd
            @stdout = stdout
            @stderr = stderr
            print "#{@cmd}: stdout: #{@stdout}, stderr: #{@stderr}\n"
        end
    end
    class PrivaliegeError < SystemError; end
end
