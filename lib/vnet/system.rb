require 'systemu'
require 'set'
require 'logger'

module Vnet::System
    @@if_prefix = 'veth'
    @@exec_counter = 0
    @@getns_inited = false
    @@getif_inited = false
    @@getbr_inited = false
    @@nss = Hash.new
    @@ifs = Hash.new
    @@brs = Hash.new

    module_function
    def if_prefix
        @@if_prefix
    end

    def execinNamespace(cmd, namespace)
        @@exec_counter += 1
        Vnet::log.info("exec(#{@@exec_counter}) #{namespace}: #{cmd}")

        if namespace == :default
            status, stdout, stderr = systemu(cmd)
        elsif getns.include? namespace
            cmd = "ip netns exec #{namespace} #{cmd}"
            status, stdout, stderr = systemu("ip netns exec #{namespace} #{cmd}")
        else
            raise ArgumentError, "invalid namespace: #{namespace}"
        end
        if not status.success?
            raise ExecError.new(cmd, stdout, stderr)
        end
        Vnet::log.info("stderr:\t#{stderr.strip.split("\n").join("\n\t")}") if stderr.size > 0
        Vnet::log.info("stdout:\t#{stdout.strip.split("\n").join("\n\t")}") if stdout.size > 0
        stdout.strip
    end

    def exec(cmd)
        execinNamespace(cmd, :default)
    end

    def clear()
        Vnet::log.debug("clear called.")
        @@getns_inited = false
        @@getif_inited = false
        @@getbr_inited = false

        while getif.size > 0
            ifname = getif.keys[0]
            delif(ifname)
        end
        getbr.keys.each do |br|
            Vnet::log.debug("delbr: #{br}")
            delbr(br)
        end
        getns.keys.each do |ns|
            Vnet::log.debug("delete netns: #{ns}")
            delns(ns)
        end

        @@nss = Hash.new
        @@ifs = Hash.new
        @@brs = Hash.new

        addns :default
    end

    def getns(flag=@@getns_inited)
        unless flag
            exec("ip netns list").split.each do |ns|
                ns = ns.to_sym
                @@nss[ns] = []
            end
            @@nss[:default] = []
            @@getns_inited = true
        end
        @@nss
    end

    def addns(name)
        if getns.keys.include? name
            raise ArgumentError, "namespace: #{name} is already created."
        end
        unless name == :default
            exec "ip netns add #{name}"
        end
        @@nss[name] = []
        execinNamespace "ip link set lo up", name
        execinNamespace "echo 1 > /proc/sys/net/ipv4/ip_forward", name
    end

    def delns(name)
        if name == :default
            return
        end
        unless getns.keys.include? name
            raise ArgumentError, "namespace: #{name} is not created."
        end
        exec "ip netns delete #{name}"
        @@nss.delete(name)
    end

    def setns(interface, namespace)
        if namespace == :default
            raise ArgumentError, "set namespace to default is not allowed."
        end
        unless getns.keys.include? namespace
            raise ArgumentError, "no namespace: #{namespace}"
        end
        unless getif.keys.include? interface
            raise ArgumentError, "no interface: #{interface}"
        end
        ns = @@ifs[interface][:ns]
        execinNamespace "ip link set #{interface} netns #{namespace}", ns
        execinNamespace "ip link set #{interface} up", namespace
        @@nss[ns].delete(interface)
        @@ifs[interface][:ns] = namespace
        @@nss[namespace] << interface
    end

    def getif(flag=@@getif_inited)
        Vnet::log.debug("getif called.")
        if not flag
            cmd = %Q?ip addr | gawk '/^[0-9]+: #{@@if_prefix}/{ \
    id=$1; \
    ifname=$2; \
    sub(":","",ifname); \
    printf("\\n%d %s ", id, ifname); \
} \
$1 == "inet" && ifname ~ /^#{@@if_prefix}/{ \
    printf("%s ",$2) \
}'?
            getns(flag).keys.each do |ns|
                execinNamespace(cmd, ns).strip.split("\n").each do |line|
                    line = line.split.map{|word| word.strip}
                    id = line[0].to_i
                    ifname = line[1].to_sym
                    if not @@ifs[ifname]
                        @@ifs[ifname] = Hash.new
                    end
                    @@ifs[ifname][:id] = id
                    @@ifs[ifname][:ns] = ns
                    @@nss[ns] << ifname
                    @@ifs[ifname][:addrs] = line[2..-1]
                end
            end

            id2ifname = Hash.new
            @@ifs.each do |ifname, value|
                cmd = %Q!ethtool -S #{ifname} | \
gawk -v i=#{ifname} '$1 == "peer_ifindex:"{ \
print $2}'!
                peerid = execinNamespace(cmd, value[:ns]).strip.to_i
                if id2ifname.include? peerid
                    @@ifs[ifname][:peer] = id2ifname[peerid]
                    @@ifs[id2ifname[peerid]][:peer] = ifname
                end
                id2ifname[value[:id]] = ifname
            end

            @@getif_inited = true
        end
        @@ifs
    end

    def addif(namex, namey)
        if getif.keys.include? namex or getif.keys.include? namey
            raise ArgumentError, "interface #{namex} or #{namey} is \
already created."
        end
        exec "ip link add #{namex} type veth peer name #{namey}"
        exec "ip link set #{namex} up"
        exec "ip link set #{namey} up"
        @@ifs[namex] = Hash.new
        @@ifs[namex][:ns] = :default
        @@ifs[namex][:peer] = namey
        @@ifs[namey] = Hash.new
        @@ifs[namey][:ns] = :default
        @@ifs[namey][:peer] = namex
    end

    def delif(name)
        unless getif.keys.include? name
            raise ArgumentError, "interface #{name} is not created."
        end
        execinNamespace "ip link delete #{name}", getif[name][:ns]
        if @@ifs[name][:peer]
            @@ifs.delete(@@ifs[name][:peer])
        end
        @@ifs.delete(name)
    end

    def setaddr(ifname, addr)
        unless getif.keys.include? ifname
            raise ArgumentError, "interface #{ifname} is not created."
        end
        execinNamespace "ip addr add #{addr} dev #{ifname}", getif[ifname][:ns]
        if @@ifs[ifname][:addrs]
            @@ifs[ifname][:addrs] << addr
        else
            @@ifs[ifname][:addrs] = [addr]
        end
    end

    def getbr(flag=@@getbr_inited)
        Vnet::log.debug("getbr called.")
        if not flag
            cmd = %Q!brctl show |\
gawk 'NR>1 && NF > 2{br=$1; veth="-"; if(NF == 4) veth=$4; print br, veth}\
NR>1 && NF == 1{print br, $1}'!
            exec(cmd).split.each_slice(2) do |br, ifname|
                br = br.to_sym
                ifname = ifname.to_sym
                if @@brs.key? br
                    @@brs[br] << ifname
                else
                    if ifname == :-
                        @@brs[br] = []
                    else
                        @@brs[br] = [ifname]
                    end
                end
            end
            @@getbr_inited = true
        end
        @@brs
    end

    def addbr(name)
        if getbr.keys.include? name
            raise ArgumentError, "bridge: #{name} is already created."
        end
        exec "brctl addbr #{name}"
        exec "ip link set #{name} up"
        @@brs[name] = []
    end

    def delbr(name)
        unless getbr.keys.include? name
            raise ArgumentError, "bridge: #{name} is not created."
        end
        exec "ip link set #{name} down; brctl delbr #{name}"
        @@brs.delete(name)
    end

    def setbr(ifname, brname)
        unless getbr.keys.include? brname
            raise ArgumentError, "no bridge: #{brname}"
        end
        unless getif.keys.include? ifname
            raise ArgumentError, "no interface: #{ifname}"
        end

        br = getif[ifname][:br]
        if br
            exec "brctl delif #{br} #{ifname}"
            getbr[br].delete ifname
        end
        exec "brctl addif #{brname} #{ifname}"
        getif[ifname][:br] = brname
        getbr[brname] << ifname
    end
end
