module Vnet
    class IPAddr
        def initialize(*arg)
            if arg.size == 1 and arg[0].class == String
                arg = arg[0].split('/')
                #convert to Fixnum
                @addr = ipstr2dex(arg[0])
                if arg.size == 1
                    @mask = cidr2dex(32)
                else
                    @mask = cidr2dex(arg[1].to_i)
                end
            elsif arg.size == 2
               @addr = arg[0]
               @mask = cidr2dex(arg[1])
            end
        end

        def ipstr2dex(str)
            raise ArgumentError, "invalid IPv4 address format" if not /^(\d{1,3}.){3}(\d{1,3})$/ =~ str
            str.split('.').map{|i| i.to_i}.reverse.each_with_index.inject(0){|sum, i| sum + i[0] * 2 ** (8*i[1])}
        end

        def dex2ipstr(dex)
            ret = Array.new 
            ret << (dex / 2 ** 24)
            dex = dex % 2 ** 24
            ret << dex / 2 ** 16
            dex = dex % 2 ** 16
            ret << dex / 2 ** 8
            dex = dex % 2 ** 8
            ret << dex
            ret.join('.')
        end

        def cidr2dex(cidr)
            raise ArgumentError, "invalid cidr" if cidr < 0 || cidr > 32
            cidr.times.inject([]){|sum, i| sum << 1}.join.to_i(2) << (32 - cidr)
        end

        def cidr(mask=nil)
            mask = @mask if not mask
            cidr = 0
            i = 0
            bit = 32
            loop do 
                if 0 < ((1<<i) & mask)
                    return bit - i
                elsif bit <= i
                    return 0
                end
                i += 1
            end
        end

        def host_count
            return 2 ** (32 - self.cidr)
        end

        def network(cidr=nil)
            return IPAddr.new(@addr & @mask, self.cidr)
        end

        def broadcast(cidr=nil)
            return self.network + (host_count - 1)
        end

        def include?(o)
            (@mask & o.addr) == (@mask & @addr)
        end

        def +(o)
            return IPAddr.new(@addr+o, self.cidr)
        end

        def -(o)
            return IPAddr.new(@addr-o, self.cidr)
        end

        def next
            return IPAddr.new(@addr+1, self.cidr)
        end

        def to_s
            return "%s/%d" % [dex2ipstr(@addr), cidr]
        end

        def host
            return dex2ipstr(@addr)
        end

        attr_accessor :addr, :mask
    end

    class Network
        def initialize(addr)
            @ip = Vnet::IPAddr.new addr
            raise "#{addr} is not a network addr" if @ip.to_s != @ip.network.to_s
            @nextip = @ip
        end

        def nextaddr
            raise SystemError, "ipaddr excess" if not @ip.include? @nextip.next
            raise SystemError, "ipaddr excess" if @ip.broadcast.to_s == @nextip.next.to_s
            @nextip = @nextip.next
        end

        def to_s
            @ip.to_s
        end

        attr_accessor :ip
    end
end
