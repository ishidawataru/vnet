require 'systemu'
require 'set'
require 'logger'

module Vnet

    @@log = Logger.new(STDOUT)
    @@log.level = Logger::INFO
    @@log.formatter = proc do |severity, datetime, progname, msg|
        "#{msg}\n"
    end
    @@veth_counter = 0
    @@loaded = false

    def getNextVethname
        ret = "#{System::if_prefix}#{@@veth_counter}".to_sym
        @@veth_counter += 1
        ret
    end 

    @@nss = Hash.new
    @@veths = Hash.new
    @@brs = Hash.new

    module_function

    def log
        @@log
    end

    def nss
        @@nss
    end

    def veths
        @@veths
    end

    def brs
        @@brs
    end

    def load(flag=@@loaded)
        unless flag
            log.info("load system.")
            @@nss = Hash.new
            @@veths = Hash.new
            @@brs = Hash.new
            System.getif.each do |k, v|
                Veth.new k
            end
            System.getns.each do |k, v|
                Namespace.new(k, true)
            end
            System.getbr.each do |k, v|
                Bridge.new(k, true)
            end
            @@veth_counter = @@veths.keys.map{|ifname| 
                ifname.to_s[System::if_prefix.size..-1].to_i
            }.sort[-1]+1 if @@veths.keys.size > 0
            @@loaded = true
        else
            log.info("already loaded")
        end
    end

    def clear
        #even when cleared default namespace remains
        Quagga.clear
        Switch.clear
        System::clear
        @@nss = Hash.new
        System.getns.each do |k, v|
            Namespace.new(k, true)
        end
        @@veths = Hash.new
        @@brs = Hash.new
        @@veth_counter = 0
        @@loaded = true
    end

    def mkLink
        namex = getNextVethname
        namey = getNextVethname
        System::addif(namex, namey)
        vethx = Veth.new namex
        vethy = Veth.new namey
        [vethx, vethy]
    end

    class Namespace
        def initialize(name='', dry=false)
            name = name.to_sym
            @@nss[name] = self
            @name = name
            System::addns(@name) unless dry
        end

        def addif(veth)
            raise ArgumentError, "arg should be Vnet::Veth" \
                if veth.class != Vnet::Veth
            System::setns(veth.name, @name)
        end

        def getif
            System::getns[@name].map{|ifname| @@veths[ifname]}
        end
        attr_reader :name
    end

    class Veth
        def initialize(name)
            @name = name.to_sym
            @@veths[@name] = self
            @addrs = []
            addrs = System::getif[@name][:addrs]
            if addrs
                addrs.each do |addr|
                    @addrs << Vnet::IPAddr.new(addr)
                end
            end
        end
        def peer
            @@veths[System::getif[@name][:peer]]
        end
        def getns
            @@nss[System::getif[@name][:ns]]
        end

        def getbr
            @@brs[System::getif[@name][:br]]
        end

        def setns(namespace)
            raise ArgumentError, "arg should be Vnet::Namespace" \
                if namespace.class != Vnet::Namespace
            System::setns(@name, namespace.name)
        end

        def setaddr(addr, dry=false)
            raise ArgumentError, "arg should be IPAddr" \
                if addr.class != Vnet::IPAddr
            System::setaddr(@name, addr.to_s)
            @addrs << addr
        end
        attr_reader :name, :addrs
    end

    # TODO: can create Linux network bridge in Linux network namespace
    class Bridge
        def initialize(name, dry=false)
            @name = name
            @@brs[@name] = self
            System::addbr name unless dry
        end

        def addif(veth)
            raise ArgumentError, "arg should be Vnet::Veth" \
                if veth.class != Vnet::Veth
            System::setbr(veth.name, @name)
        end

        def getif
            System::getbr[@name].map{|ifname| @@veths[ifname]}
        end
        attr_reader :name
    end
end
