class Veth 
    def set_bandwidth(bandwidth)
        if @namespace
            Vnet.execinNamespace "tc qdisc add dev #{@name} root tbf \
limit 20kb burst 20kb rate #{bandwidth}", @namespace.name
        else
            Vnet.execinNamespace "tc qdisc add dev #{@name} root tbf \
limit 20kb burst 20kb rate #{bandwidth}"
        end

        @bandwidth = bandwidth
    end
end        

