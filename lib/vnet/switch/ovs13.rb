include Vnet

module Vnet::Switch::OVS13
    @@EXECPATH = nil
    @@RUNPATH = "/usr/local/var/run/openvswitch"
    `mkdir -p #{@@RUNPATH}`
    @@setup_flag = false

    module_function
    def getbr
        cmd = %Q!ovs-vsctl show | awk '
/Bridge/{gsub(/"/, "", $2); print $2}'!
        System::exec(cmd).split.map!{|br| br.to_sym}
    end

    def getif(br=nil, with=nil)
        brs = []
        if br == nil
            brs = getbr
        else
            raise ArgumentError, "#{br} is not an OVS bridge" if not getbr.include? br
            brs = [br]
        end

        ret = []
        brs.each do |br|
            cmd = %Q!ovs-ofctl dump-ports-desc #{br} -O OpenFlow13 \
| awk -F '[ ()]' '/^ [0-9]+\\(#{System::if_prefix}[0-9]+\\)/{
    print $3, $2
}'!
            System::exec(cmd).split.each_slice(2) do |ifname, port|
                if with == nil
                    ret << ifname.to_sym
                elsif with == :port
                    ret << [ifname.to_sym, port.to_i]
                end
            end
        end
        ret
    end

    def start_ovsdb
        cmd = "service openvswitch-switch start"
        System::exec cmd
    end

    def check
    end

    def clear
        log.debug("ovs13 clear.")
        start_ovsdb()
        brs = (System::exec 'ovs-vsctl list-br').split
        brs.each do |br|
            System::exec "ovs-vsctl del-br #{br}"
        end
        cmd = "service openvswitch-switch stop"
        System::exec cmd
        @@setup_flag = false
    end

    def load
        ret = Hash.new
        getbr.each do |brname|
            ret[name] = Bridge.new name
            getif(name, :port).each do |vethname, port|
                ret[name].addif veths[vethname]
            end
        end
        ret
    end

    class Bridge < Vnet::Switch::Bridge
        include Vnet::Switch::OVS13

        def initialize(name)
            super name
        end

        def ofctl(cmd)
            cmd = "ovs-ofctl -O OpenFlow13 #{cmd} #{@name}"
            System::exec cmd
        end

        def run
            start_ovsdb unless @@setup_flag
            @@setup_flag = true
            System::exec "ovs-vsctl add-br #{@name}"
            System::exec "ovs-vsctl set bridge #{@name} protocols=OpenFlow13"
            if @ctrladdr
                System::exec "ovs-vsctl set-controller #{@name} \
tcp:#{@ctrladdr}:#{@ctrlport}"
                System::exec "ovs-vsctl set-fail-mode #{@name} secure"
            end
            @veths.each do |k, v|
                System::exec "ovs-vsctl add-port #{@name} #{k}"
            end
        end
    end
end
