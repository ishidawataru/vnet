include Vnet

module Vnet::Switch::CPqD
    @@EXECPATH = nil
    @@PIDPATH = "/var/run/cpqd"
    `mkdir -p #{@@PIDPATH}`
    @@LOGPATH = "/var/log/cpqd"
    `mkdir -p #{@@LOGPATH}`
    @@scport_counter = 10000
    @@supported_versions = ['1.3.0']

    def getNextScport
        @@scport_counter += 1
    end
    module_function

    def check(execpath=nil)
        @@EXECPATH = execpath
        cmd = "ofprotocol --version | awk '{print $2}'"
        cmd = "#{@@EXECPATH}/secchan/#{cmd}" if @@EXECPATH
        version = System::exec(cmd)
        if @@supported_versions.include? version
            return true
        else
            return false
        end
    end

    def clear
        Dir.glob("#{@@PIDPATH}/*.pid") do |file|
            pid = File.open(file).read.strip.to_i
            begin
                log.debug("kill #{pid}")
                Process.kill(:KILL, pid)
            ensure
                log.debug("remove pidfile(#{file}).")
                File.delete file
            end
        end
        @@scport_counter = 10000
    end

    def load
        ret = Hash.new
        log.debug("load cpqd.")
        Vnet::load
        cmds = Dir.glob("#{@@PIDPATH}/*.pid").map do |file|
            pid = File.open(file).read.strip
            cmd = "ps ax |\
awk '/^ *#{pid}/{for(i=5;i<NF+1;i++)printf(\"%s \",$i)}'"
            cmd = System::exec(cmd)
        end
        cmds.each do |cmd|
            if %r{^ofdatapath.*\/(.*)_dp\.pid.*--interfaces=((veth[0-9]+ )+)}\
                .match(cmd)
                name = $1.to_sym
                interfaces = $2
                ret[name] = Bridge.new name
                interfaces.split.map{|name| name.strip.to_sym}.each do |veth|
                    if Vnet::veths.keys.include? veth
                        ret[name].addif veths[veth]
                    else
                        raise "fail to find #{veth.name} which is associated \
with #{br.name}. Better to kill #{br.name}"
                    end
                end
            end
        end
        cmds.each do |cmd|
            if %r{^ofprotocol.*\/(.*)_sc.*tcp:(.*):(.*) tcp:(.*):6633}.\
                match(cmd)
                ret[$1.to_sym].setController IPAddr.new($4)
                ret[$1.to_sym].scport = $3.to_i
            end
        end
        @@scport_counter = ret.values.map{|sw| sw.scport}.compact.sort[-1]
        ret
    end

    class Bridge < Vnet::Switch::Bridge
        include Vnet::Switch::CPqD

        def initialize(name)
            super name
            @scport = getNextScport
            @logfile = "#{@@LOGPATH}/#{@name}.log"
            @dppidfile = "#{@@PIDPATH}/#{@name}_dp.pid"
            @scpidfile = "#{@@PIDPATH}/#{@name}_sc.pid"
            @socketfile = "#{@@PIDPATH}/#{@name}.sock"
        end

        def ofctl(cmd)
            cmd = "ovs-ofctl -O OpenFlow13 #{cmd} unix:#{@socketfile}"
            System::exec cmd
        end

        def run
            cmd = "ofdatapath --pidfile=#{@dppidfile} -D --interfaces=\
#{@vethsarray.join(',')} ptcp:#{@scport}"
            cmd = "#{@@EXECPATH}/udatapath/#{cmd}" if @@EXECPATH
            System::exec(cmd)
            if @ctrladdr
                cmd = "ofprotocol --pidfile=#{@scpidfile} -D --log-file=\
#{@logfile} --inactivity-probe=90 --listen=punix:#{@socketfile} \
tcp:localhost:#{@scport} tcp:#{@ctrladdr}:#{@ctrlport}"
                cmd = "#{@@EXECPATH}/secchan/#{cmd}" if @@EXECPATH
                System::exec(cmd)
            end
        end
        attr_accessor :scport
    end
end
