include Vnet

module Vnet
    class Veth
        attr_accessor :sw, :swport
    end
end

module Vnet::Switch
#    @@supported_switch_type = [:ovs, :ovs13, :cpqd]
    @@supported_switch_type = [:cpqd]
    @@brs = Hash.new

    module_function
    def getbr
        @@brs
    end

    def check(type)
        unless @@supported_switch_type.include? type
            raise ArgumentError, "type should be #{@@supported_switch_type}"
        end
    end

    def mkBridge(type, name)
        unless @@supported_switch_type.include? type
            raise ArgumentError, "type should be #{@@supported_switch_type}"
        end

        if @@brs.keys.include? name
            raise ArgumentError, "Bridge named #{name} already exists!"
        end

        if type == :ovs
            OVS::Bridge.new name
        elsif type == :ovs13
            OVS13::Bridge.new name
        elsif type == :cpqd
            CPqD::Bridge.new name
        end
    end

    def clear
        types = [CPqD]
        types.each do |type|
            type.clear
        end
    end

    def run
        @@brs.each do |k,v|
            log.debug("run #{k}.")
            v.run
        end
    end

    class Bridge
        include Vnet::Switch
        def initialize(name)
            @name = name
            @veths = Hash.new
            Switch::getbr[@name] = self
            #TODO check this implmentation is also ok with ovs
            @portnum = 1
            @vethsarray = Array.new
        end

        def dumpflows
            ofctl("dump-flows").rstrip.split(/\r?\n/).map{|line| line.chomp}
        end

        def setController(ip)
            raise ArgumentError, "arg should be IPAddr" if ip.class != IPAddr
            @ctrladdr = ip.host
            @ctrlport = 6633
        end

        def addif(veth)
            unless veth.class == Veth
                raise ArgumentError, "arg should be Vnet::Veth"
            end
            unless veth.getns.name == :default
                raise ArgumentError, "#{veth.name}(#{veth.getns.name}) must in \
default ns."
            end
            @veths[veth.name] = veth
            @vethsarray.push(veth.name)
            veth.sw = self
            veth.swport = @portnum
            @portnum += 1
            veth
        end

        def getif
            @veths
        end

        def run
            raise "Abstraction method. Implement run() in sub-Class."
        end

        attr_reader :name, :ctrladdr, :ctrladdr
    end
end
