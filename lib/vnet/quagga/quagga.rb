require 'erb'
include Vnet

module Vnet::Quagga
    @@EXECPATH = nil
    @@SOCKETPATH = "/var/run/quagga"
    `mkdir -p #{@@SOCKETPATH}`
    @@LOGPATH = "/var/log/quagga"
    `mkdir -p #{@@LOGPATH}`
    @@CONFPATH = "/etc/quagga"
    `mkdir -p #{@@CONFPATH}`
    @@TEMPLATEPATH = File.join File.dirname(__FILE__), "template"
    @@supported_versions = ['0.99.22.3']
    @@loaded = false

    module_function

    def check(execpath=nil)
        @@EXECPATH = execpath
        cmd = "zebra --version | awk '/zebra/{print $3}'"
        cmd = "#{@@EXECPATH}/#{cmd}" if @@EXECPATH
        version = System::exec(cmd)
        unless @@supported_versions.include? version
            log.error("vnet doesn't support quagga version #{version}")
            log.error("supported versions: #{@@supported_versions}")
            return false
        end
        cmd = "zebra --help | grep '\-x'"
        cmd = "#{@@EXECPATH}/#{cmd}" if @@EXECPATH
        output = System::exec(cmd)
        unless output.size > 0
            log.error("your quagga seems not to support multi-instance.")
            log.error("check quagga has option -x.")
            return false
        end
        return true
    end

    def clear()
        log.debug("quagga clear.")
        Dir.glob("#{@@SOCKETPATH}/*/*.pid") do |file|
            pid = File.open(file).read.strip.to_i
            begin
                log.debug("kill #{pid}")
                Process.kill(:KILL, pid)
            rescue
                log.debug("failed to kill #{pid}")
            ensure
                log.debug("remove pidfile(#{file}).")
                FileUtils.rm(file)
            end
        end
        @@loaded = true
    end

    class Daemon
        include Vnet::Quagga
        def run
            `mkdir -p #{@@SOCKETPATH}/#{@ns}`
            `chown quagga:quagga #{@@SOCKETPATH}/#{@ns}`
            `touch #{@@SOCKETPATH}/#{@ns}/#{@daemon}.vty`
            `mkdir -p #{@@CONFPATH}/#{@ns}`
            `touch #{@@CONFPATH}/#{@ns}/#{@daemon}.conf`
            `chown quagga:quagga #{@@LOGPATH}/#{@ns}`
            `mkdir -p #{@@LOGPATH}/#{@ns}`

            raise SystemError, "daemon is not specified" if not defined? @daemon
            cmd = "#{@daemon} -d -x #{@@SOCKETPATH}/#{@ns}/#{@daemon}.vty \
-i #{@@SOCKETPATH}/#{@ns}/#{@daemon}.pid \
-f #{@@CONFPATH}/#{@ns}/#{@daemon}.conf"
            cmd = "#{@@EXECPATH}/#{cmd}" if @@EXECPATH
            #enable api server for ospfd
            cmd = "#{cmd} -a" if @daemon == :ospfd
            System::execinNamespace cmd, @ns
        end

        def write
            @logfile = "#{@@LOGPATH}/#{@ns}/#{@daemon}.log"
            raise SystemError, "daemon is not specified" if not defined? @daemon

            erb = File.open("#{@@TEMPLATEPATH}/#{@daemon}.erb") do |io|
                erb = ERB.new io.read, nil, '-'
            end

            result = erb.result binding

            `mkdir -p #{@@CONFPATH}/#{@ns}`
            File.open("#{@@CONFPATH}/#{@ns}/#{@daemon}.conf", 'w') do |io|
                io.write result
            end
        end

        def die
            file = "#{@@SOCKETPATH}/#{@ns}/#{@daemon}.pid"
            pid = File.open(file).read.strip.to_i
            begin
                log.debug("kill #{pid}")
                Process.kill(:KILL, pid)
            ensure
                log.debug("remove pidfile(#{file}).")
                FileUtils.rm(file)
            end
        end

        def redistribute(*protos)
            @redistribute = []
            protos.each do |proto|
                unless [:kernel, :static, :connected, :ospf, :bgp].any?{|p| p == proto}
                    raise ArgumentError, "#{proto} is invalid protocol to redistribute"
                end
                @redistribute << proto.to_s
            end
        end
    end

    class Zebra < Daemon
        def initialize(ns)
            @daemon = :zebra
            @ns = ns
            @hostname = @ns
            @password = :zebra
        end
    end

    class OSPFd < Daemon
        def initialize(ns)
            @daemon = :ospfd
            @ns = ns
            @hostname = @ns
            @password = :zebra
            @ifs = Array.new
            @networks = Array.new
        end

        def addif(veth, cost=10, passive=false)
            raise ArgumentError, "arg should be Vnet::Veth" if veth.class != Vnet::Veth
            raise ArgumentError, "#{veth.name} doesn't belong to #{@ns}" if veth.getns.name != @ns
            @ifs << {:name => veth.name, :passive => passive, :cost => cost}
        end

        def addnw(nw)
            raise ArgumentError, "arg should be Vnet::IPAddr" if nw.class != Vnet::IPAddr
            @networks << {:addr => nw.to_s, :area => 0}
        end

        def id=(id)
            raise ArgumentError, "arg should be Vnet::IPAddr" if id.class != Vnet::IPAddr
            @id = id.host
        end
    end

    class BGPd <Daemon
        def initialize(ns, asn, timer=5)
            @daemon = 'bgpd'
            @ns = ns
            @asn = asn
            @hostname = @ns
            @password = 'zebra'
            @networks = Array.new
            @neighbors = Array.new
            @timer = timer
        end

        def addnw(nw)
            raise ArgumentError, "arg should be Vnet::IPAddr" if nw.class != Vnet::IPAddr
            @networks << {:addr => nw.to_s}
        end

        def addneighbor(addr, asn)
            raise ArgumentError, "arg should be Vnet::IPAddr" if addr.class != Vnet::IPAddr
            @neighbors << {:addr => addr.to_s.split('/')[0], :asn => asn, :timer => @timer}
        end

        attr_accessor :asn
    end
end
