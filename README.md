Vnet
===

## What's this?

- Linux上での仮想ネットワーク構築を補助してくれるライブラリ
	- Linux Network Namespaceを利用したネットワークスライスの作成
	- vethによる仮想リンクの作成
	- Linux Bridgeによる仮想L2スイッチの作成
	- ovs, ofsoftswitch13に対応し、OF環境の構築を支援
	- quaggaに対応し仮想ネットワーク内で仮想ルータを稼働させることができる
		- OSPFd, BGPdに対応
		- マルチインスタンスを可能にするパッチを当てたquaggaが必要
	- tcコマンドを利用した仮想リンクの帯域、遅延の調整が可能
- [TODO]dotファイル出力
	- 可視化
- [TODO]yamlファイル入出力
	- yamlファイルから仮想ネットワークを構築してくれる(仮想環境のバージョン管理)

## Usage

	require 'vnet'
	
	Vnet::clear
	
	#create virutal link
	veth1, veth2 = Vnet::mkLink
	
	#create Linux Network Namespace
	ns01 = Vnet::Namespace.new :ns01
	ns02 = Vnet::Namespace.new :ns02
	
	#add interface to namespace
	ns01.addif veth1
	ns02.addif veth2
	
	#set ip address to veth
	nw = Vnet::Network.new("192.168.0.0/24")
	addr1 = nw.nextaddr # this creates Vnet::IPAddr.new(192.168.0.1/24)
	addr2 = nw.nextaddr # this creates Vnet::IPAddr.new(192.168.0.2/24)
	vnet1.setaddr addr1
	vnet2.setaddr addr2

for more example, look at spec files.

## Component

- util.rb : LinuxとのInterface
	- getで始まる関数群
		- 実際にipコマンド等を叩いて状態を把握する
		- 実行コストが高いのでcore.rb内でステートを持って、なるべく呼び出し回数を減らす
	- getns : Namespaceの取得
	- getif : Interface名の取得
	- getaddr : Interfaceアドレスの取得
	- getpair : 対になったvethインターフェイス名の取得
	- プロセスを扱う(主にkillする)関数も欲しい
- core.rb : 基本構造体
	- `class Veth`  
		- set_namespace()
		- addaddr()
	- `class Namespace`
		- addif()
	- `class Bridge`	 : Linux Bridge 現段階ではNamespaceには所属させられない
		- addif()
- quagga.rb : quaggaのコンフィグを生成し、起動
	- どこに実行ファイルがあるか、どこに設定ファイルを置くか、設定する必要がある
	- マルチインスタンスのパッチが当たっているか確認するすべも必要
	- module vnet::quagga module zebra, module bgp, module ospf
	- 何かにincludeして使う？
		 - zebra_config(), bgp_config(), ospf_config()を提供？
- switch.rb : 
	- どこに実行ファイルがあるか、どこに設定ファイルを置くか、設定する必要がある

- network.rb :
	- `class IPAddr`
		- なぞの使い勝手の悪さだったgem IPAddrの改良版
	- `class Network`

## Installation

Add this line to your application's Gemfile:

	gem 'vnet'

And then execute:

	$ bundle

Or install it yourself as:

	$ gem install vnet
## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
